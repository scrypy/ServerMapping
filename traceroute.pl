#!/usr/bin/perl

#@sites=('google.com','facebook.com','yahoo.com','reddit.com','aol.com',
#'xkcd.com','tutorialspoint.com','msnbc.com','pbs.org','wikipedia.org',
#'twitter.com','instagram.com','snapchat.com','youtube.com','netflix.com',
#'msn.com','linkedin.com','amazon.com','ebay.com','pinterest.com','wordpress.com',
#'imgur.com','apple.com','imdb.com','github.com','cnn.com','dropbox.com',
#'slashdot.com','');
@sites=('google.com','facebook.com');
my $max=0;#max ttl to shorten data retrieval
my $filename = './5000mapping.csv';
open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";
open(my $list, '<', './URLlist.txt') or die "Could not open list file $!";
print $fh "source;target;\n";
my $progress=0;
while(my $site = <$list>){
  chomp($site);
  printf "---------------------------------------------\n";
  printf "------------------$site----------------------\n";
  printf "-------------------$progress/4999------------\n";

  $count=0;
  $source="192.168.1.1";
  open(TR,"traceroute -m 30 $site |") || die "Failed: $!\n";
  while ( $line=<TR> )
  {
    chomp($line);
    if ($line =~ m/\((.*?)\)/){
      if($count>1){
        printf $fh "$source;$1;\n";
        $source=$1;
        printf "match $1\n";
      }
      $count++;
    }
    elsif($line=~ /\*/){
      #blocked
      printf "$line\n";
    }
  }
  print $fh "$source;$site;\n";
  $progress++;
}

close $list;
close $fh;
print "done\n";#25
#=~/\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/)
