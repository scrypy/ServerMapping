<h1>Server Mapping</h1>
<div>
Mapping internet traffic through traceroutes implemented by a Perl script on 5000 different websites.

<h2>Made with:</h2>
<ul>
<li>JS</li>
<li>Perl</li>
<li>Gephi Graphing utilities</li>
</ul>
</div>
